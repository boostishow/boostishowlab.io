# Comment contribuer

## Quels tyes de fichiers sont acceptés

Nous acceptons les séries qui ont une qualité **supérieure ou égale au 720p**. Tout ce qui est en dessous de cette qualité sera refusé (sauf si la série est introuvable en qualité supérieure).

Seul les liens **uptobox** et **1fichier** sont acceptés.

## Tutoriel

Faire un pull request en créant un fichier *.md* dans le dossier `./content`.

Le nom du fichier doit être *slugifier*.

### Automatiquement

Il suffit d'utiliser le programme [exuptobox](https://gitlab.com/boostishow/exuptobox) (Ce programme fonctionne juste avec les liens uptobox).

Les fichiers doivent être bien nommé pour que le programme marche (eg. *Altered.Carbon.S01E10.MULTi.1080p.WEBRip.x264.mkv*).

Utilisation:
```
$ exuptobox "<links>"
```

Exemple d'utilisation:
```
$ exuptobox "https://uptobox.com/000000000000 https://uptobox.com/000000000000"
```

### Manuellement

Il faut respecter l'exemple ci-dessous. **Merci de bien faire attention aux espaces à la fin des lignes**.
```
---
title: "Ma série"
created_at: 2021-04-20
updated_at: 2021-04-20
---

## Saison 1

1080p MULTi

- Episode 1  
[https://uptobox.com/000111222333](https://uptobox.com/000111222333)  
- Episode 2  
[https://uptobox.com/000111222333](https://uptobox.com/000111222333)  
- Episode 3  
[https://uptobox.com/000111222333](https://uptobox.com/000111222333) 

720p VOST

- Episode 1  
[https://uptobox.com/000111222333](https://uptobox.com/000111222333)  
- Episode 2  
[https://uptobox.com/000111222333](https://uptobox.com/000111222333)  
- Episode 3  
[https://uptobox.com/000111222333](https://uptobox.com/000111222333) 

## Saison 2

1080p MULTi

- Episode 1  
[https://uptobox.com/000111222333](https://uptobox.com/000111222333)  

720p VOST

- Episode 1  
[https://uptobox.com/000111222333](https://uptobox.com/000111222333)  

```

Si le fichier ne respecte pas les règles ci-dessus, le pull request sera refusé.
