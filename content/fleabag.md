---
title: "Fleabag"
created_at: 2021-04-12
updated_at: 2021-04-19
---

## Saison 1

1080p MULTi
- Episode 1  
[https://uptobox.com/tx7f68xnvpqg](https://uptobox.com/tx7f68xnvpqg)  
- Episode 2  
[https://uptobox.com/g0h5zr7jn8ep](https://uptobox.com/g0h5zr7jn8ep)  
- Episode 3  
[https://uptobox.com/hdaq98imjmxo](https://uptobox.com/hdaq98imjmxo)  
- Episode 4  
[https://uptobox.com/b2yf7l3s4klk](https://uptobox.com/b2yf7l3s4klk)  
- Episode 5  
[https://uptobox.com/dghiqa4fig1d](https://uptobox.com/dghiqa4fig1d)  
- Episode 6  
[https://uptobox.com/k0cjq6l0eqm5](https://uptobox.com/k0cjq6l0eqm5)  

1080p VOSTFR
- Episode 1  
[https://uptobox.com/dobppaenu1k3](https://uptobox.com/dobppaenu1k3)  
- Episode 2  
[https://uptobox.com/tkflariqq1ee](https://uptobox.com/tkflariqq1ee)  
- Episode 3  
[https://uptobox.com/tlnix4rh0uyj](https://uptobox.com/tlnix4rh0uyj)  
- Episode 4  
[https://uptobox.com/ba647bjteb95](https://uptobox.com/ba647bjteb95)  
- Episode 5  
[https://uptobox.com/4r2tlytdgmtt](https://uptobox.com/4r2tlytdgmtt)  
- Episode 6  
[https://uptobox.com/miu38dbfk6sk](https://uptobox.com/miu38dbfk6sk)  

## Saison 2

1080p VOSTFR
- Episode 1  
[https://uptobox.com/lfwhw5bytcaf](https://uptobox.com/lfwhw5bytcaf)  
- Episode 2  
[https://uptobox.com/fhlvsl3ffu1y](https://uptobox.com/fhlvsl3ffu1y)  
- Episode 3  
[https://uptobox.com/g44tqtllrrwx](https://uptobox.com/g44tqtllrrwx)  
- Episode 4  
[https://uptobox.com/jun57j39rrnc](https://uptobox.com/jun57j39rrnc)  
- Episode 5  
[https://uptobox.com/kno8wxhef0de](https://uptobox.com/kno8wxhef0de)  
- Episode 6  
[https://uptobox.com/vwtn31ugei5z](https://uptobox.com/vwtn31ugei5z)  

720p FRENCH
- Episode 1  
[https://uptobox.com/54gjh1wqfdo0](https://uptobox.com/54gjh1wqfdo0)  
- Episode 2  
[https://uptobox.com/vb5na7n80vfq](https://uptobox.com/vb5na7n80vfq)  
- Episode 3  
[https://uptobox.com/y1h952zumo1n](https://uptobox.com/y1h952zumo1n)  
- Episode 4  
[https://uptobox.com/db2sp5j89o1h](https://uptobox.com/db2sp5j89o1h)  
- Episode 5  
[https://uptobox.com/nc9g6iw8ifn9](https://uptobox.com/nc9g6iw8ifn9)  
- Episode 6  
[https://uptobox.com/7guwx7cafimc](https://uptobox.com/7guwx7cafimc)  
<!-- fleabag.md -->
