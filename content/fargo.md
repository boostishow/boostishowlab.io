---
title: "Fargo"
created_at: 2021-04-08
updated_at: 2021-04-08
---

## Saison 1

1080p MULTi
- Episode 1  
[https://uptobox.com/82a37udel05r](https://uptobox.com/82a37udel05r)  
- Episode 2  
[https://uptobox.com/k4eukjvr5vjc](https://uptobox.com/k4eukjvr5vjc)  
- Episode 3  
[https://uptobox.com/zlbta00kdylk](https://uptobox.com/zlbta00kdylk)  
- Episode 4  
[https://uptobox.com/1bmyqj9a0whv](https://uptobox.com/1bmyqj9a0whv)  
- Episode 5  
[https://uptobox.com/o1nufhsbf61f](https://uptobox.com/o1nufhsbf61f)  
- Episode 6  
[https://uptobox.com/k9od76y8f4o4](https://uptobox.com/k9od76y8f4o4)  
- Episode 7  
[https://uptobox.com/4yk5ea4w49ek](https://uptobox.com/4yk5ea4w49ek)  
- Episode 8  
[https://uptobox.com/6gogbiy53gbd](https://uptobox.com/6gogbiy53gbd)  
- Episode 9  
[https://uptobox.com/8q90bmv2eauu](https://uptobox.com/8q90bmv2eauu)  
- Episode 10  
[https://uptobox.com/lbwtytf0utuu](https://uptobox.com/lbwtytf0utuu)  

## Saison 2

1080p MULTi
- Episode 1  
[https://uptobox.com/0zwm7c825ho3](https://uptobox.com/0zwm7c825ho3)  
- Episode 2  
[https://uptobox.com/yf9gq5w6diun](https://uptobox.com/yf9gq5w6diun)  
- Episode 3  
[https://uptobox.com/uxjirbweb7fj](https://uptobox.com/uxjirbweb7fj)  
- Episode 4  
[https://uptobox.com/ymec3z6lpe8u](https://uptobox.com/ymec3z6lpe8u)  
- Episode 5  
[https://uptobox.com/9gcxnx37myyq](https://uptobox.com/9gcxnx37myyq)  
- Episode 6  
[https://uptobox.com/g4mjuzl5d2im](https://uptobox.com/g4mjuzl5d2im)  
- Episode 7  
[https://uptobox.com/139i5ezv4rn6](https://uptobox.com/139i5ezv4rn6)  
- Episode 8  
[https://uptobox.com/ouqty42n9vdp](https://uptobox.com/ouqty42n9vdp)  
- Episode 9  
[https://uptobox.com/cs6pmrh3b77s](https://uptobox.com/cs6pmrh3b77s)  
- Episode 10  
[https://uptobox.com/gv45qr6abafw](https://uptobox.com/gv45qr6abafw)  

## Saison 3

1080p MULTi
- Episode 1  
[https://uptobox.com/5w45a3kdljly](https://uptobox.com/5w45a3kdljly)  
- Episode 2  
[https://uptobox.com/8e6mawlhzhbd](https://uptobox.com/8e6mawlhzhbd)  
- Episode 3  
[https://uptobox.com/jqajqeulpjsk](https://uptobox.com/jqajqeulpjsk)  
- Episode 4  
[https://uptobox.com/jiw6bar9ppdj](https://uptobox.com/jiw6bar9ppdj)  
- Episode 5  
[https://uptobox.com/jyn59r13twef](https://uptobox.com/jyn59r13twef)  
- Episode 6  
[https://uptobox.com/xx4u038ehiwp](https://uptobox.com/xx4u038ehiwp)  
- Episode 7  
[https://uptobox.com/u8iwy3nrbvqj](https://uptobox.com/u8iwy3nrbvqj)  
- Episode 8  
[https://uptobox.com/0yn8jkktj99p](https://uptobox.com/0yn8jkktj99p)  
- Episode 9  
[https://uptobox.com/s8h54kfa3aeb](https://uptobox.com/s8h54kfa3aeb)  
- Episode 10  
[https://uptobox.com/jh2lx186ejro](https://uptobox.com/jh2lx186ejro)  

## Saison 4

1080p FRENCH
- Episode 1  
[https://uptobox.com/bv0uavr7frkb](https://uptobox.com/bv0uavr7frkb)  
- Episode 2  
[https://uptobox.com/srq3ymlotab5](https://uptobox.com/srq3ymlotab5)  
- Episode 3  
[https://uptobox.com/ik8dyj4pu31p](https://uptobox.com/ik8dyj4pu31p)  
- Episode 4  
[https://uptobox.com/meeihoqgdzvk](https://uptobox.com/meeihoqgdzvk)  
- Episode 5  
[https://uptobox.com/cak63mat4rr4](https://uptobox.com/cak63mat4rr4)  
- Episode 6  
[https://uptobox.com/fe8sb2ttazg0](https://uptobox.com/fe8sb2ttazg0)  
- Episode 8  
[https://uptobox.com/ujlg9nvmwqk0](https://uptobox.com/ujlg9nvmwqk0)  

720p FRENCH
- Episode 7  
[https://uptobox.com/7nf48cfrk6vl](https://uptobox.com/7nf48cfrk6vl)  
- Episode 9  
[https://uptobox.com/qzzqg4k1zht0](https://uptobox.com/qzzqg4k1zht0)  
- Episode 10  
[https://uptobox.com/sbo1dljqcjnt](https://uptobox.com/sbo1dljqcjnt)  
- Episode 11  
[https://uptobox.com/3wzw8if4ti7d](https://uptobox.com/3wzw8if4ti7d)  
