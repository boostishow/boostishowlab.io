---
title: "Charmed"
created_at: 2021-04-12
updated_at: 2021-04-12
---

## Saison 1

1080p MULTi
- Episode 1  
[https://uptobox.com/16ei8aihi6vq](https://uptobox.com/16ei8aihi6vq)  
- Episode 2  
[https://uptobox.com/asfoy6wxrwn7](https://uptobox.com/asfoy6wxrwn7)  
- Episode 3  
[https://uptobox.com/5achwyacdl6u](https://uptobox.com/5achwyacdl6u)  
- Episode 4  
[https://uptobox.com/7i17dl4tzwq0](https://uptobox.com/7i17dl4tzwq0)  
- Episode 5  
[https://uptobox.com/9p7t4zdh0agp](https://uptobox.com/9p7t4zdh0agp)  
- Episode 6  
[https://uptobox.com/j8hykpb2shwz](https://uptobox.com/j8hykpb2shwz)  
- Episode 7  
[https://uptobox.com/ogfiuomj3fsu](https://uptobox.com/ogfiuomj3fsu)  
- Episode 8  
[https://uptobox.com/ejuyiew0ja6k](https://uptobox.com/ejuyiew0ja6k)  
- Episode 9  
[https://uptobox.com/s86bco0pv1db](https://uptobox.com/s86bco0pv1db)  
- Episode 10  
[https://uptobox.com/vb2tayw3cvpt](https://uptobox.com/vb2tayw3cvpt)  
- Episode 11  
[https://uptobox.com/jctsjzb40ne7](https://uptobox.com/jctsjzb40ne7)  
- Episode 12  
[https://uptobox.com/tj242fql9lgm](https://uptobox.com/tj242fql9lgm)  
- Episode 13  
[https://uptobox.com/byd0ompg10mx](https://uptobox.com/byd0ompg10mx)  
- Episode 14  
[https://uptobox.com/ok9k74emh5se](https://uptobox.com/ok9k74emh5se)  
- Episode 15  
[https://uptobox.com/9hzll4fw6i8o](https://uptobox.com/9hzll4fw6i8o)  
- Episode 16  
[https://uptobox.com/lsto9nmv6k1q](https://uptobox.com/lsto9nmv6k1q)  
- Episode 17  
[https://uptobox.com/hhh318v9mgku](https://uptobox.com/hhh318v9mgku)  
- Episode 18  
[https://uptobox.com/sg65h9hqn6au](https://uptobox.com/sg65h9hqn6au)  
- Episode 19  
[https://uptobox.com/gc9f0xhna89d](https://uptobox.com/gc9f0xhna89d)  
- Episode 20  
[https://uptobox.com/15oeqm7h4c92](https://uptobox.com/15oeqm7h4c92)  
- Episode 21  
[https://uptobox.com/o4xwietqae95](https://uptobox.com/o4xwietqae95)  
- Episode 22  
[https://uptobox.com/ezy9n4dxs0j1](https://uptobox.com/ezy9n4dxs0j1)  

## Saison 2

1080p FRENCH
- Episode 1  
[https://uptobox.com/5my0tw4sjqtl](https://uptobox.com/5my0tw4sjqtl)  
- Episode 2  
[https://uptobox.com/7305hccrmf4o](https://uptobox.com/7305hccrmf4o)  
- Episode 3  
[https://uptobox.com/wu5o9bgsuw2a](https://uptobox.com/wu5o9bgsuw2a)  
- Episode 4  
[https://uptobox.com/rfi4vh992rn0](https://uptobox.com/rfi4vh992rn0)  
- Episode 5  
[https://uptobox.com/xxmnrj16kxc4](https://uptobox.com/xxmnrj16kxc4)  
- Episode 6  
[https://uptobox.com/voxqiulg0hch](https://uptobox.com/voxqiulg0hch)  
- Episode 7  
[https://uptobox.com/ytfnijnru6x7](https://uptobox.com/ytfnijnru6x7)  
- Episode 8  
[https://uptobox.com/nxf8t6gmril9](https://uptobox.com/nxf8t6gmril9)  
- Episode 9  
[https://uptobox.com/hawk2zqgl8xs](https://uptobox.com/hawk2zqgl8xs)  
- Episode 10  
[https://uptobox.com/v1ze1jzab0jl](https://uptobox.com/v1ze1jzab0jl)  
- Episode 11  
[https://uptobox.com/lyijb71bvo1w](https://uptobox.com/lyijb71bvo1w)  
- Episode 12  
[https://uptobox.com/ibqw028asnsi](https://uptobox.com/ibqw028asnsi)  
- Episode 13  
[https://uptobox.com/tdqai5gvicli](https://uptobox.com/tdqai5gvicli)  
- Episode 14  
[https://uptobox.com/xub887qf63jf](https://uptobox.com/xub887qf63jf)  
- Episode 15  
[https://uptobox.com/weo8oavvbg7n](https://uptobox.com/weo8oavvbg7n)  
- Episode 16  
[https://uptobox.com/o6f80q9dxsff](https://uptobox.com/o6f80q9dxsff)  
- Episode 17  
[https://uptobox.com/8xlatz9m7nsr](https://uptobox.com/8xlatz9m7nsr)  
- Episode 18  
[https://uptobox.com/58n2ocgmphz4](https://uptobox.com/58n2ocgmphz4)  
- Episode 19  
[https://uptobox.com/xsh4lo64u9lo](https://uptobox.com/xsh4lo64u9lo)  
- Episode 20  
[https://uptobox.com/iinq0ay65k5c](https://uptobox.com/iinq0ay65k5c)  
- Episode 21  
[https://uptobox.com/8n66b5vqnn0t](https://uptobox.com/8n66b5vqnn0t)  
- Episode 22  
[https://uptobox.com/ltxwpnz1nlny](https://uptobox.com/ltxwpnz1nlny)  

## Saison 3

1080p FRENCH
- Episode 1  
[https://uptobox.com/00is9ve55opl](https://uptobox.com/00is9ve55opl)  
- Episode 2  
[https://uptobox.com/hufmq59h2g0h](https://uptobox.com/hufmq59h2g0h)  
- Episode 3  
[https://uptobox.com/zpzdwn9z2h7l](https://uptobox.com/zpzdwn9z2h7l)  
- Episode 4  
[https://uptobox.com/hpekimke3jq4](https://uptobox.com/hpekimke3jq4)  
- Episode 5  
[https://uptobox.com/2maqm0ivg0fl](https://uptobox.com/2maqm0ivg0fl)  
- Episode 6  
[https://uptobox.com/8sruwooy5pjt](https://uptobox.com/8sruwooy5pjt)  
- Episode 7  
[https://uptobox.com/34picoyfw020](https://uptobox.com/34picoyfw020)  
- Episode 8  
[https://uptobox.com/0gt4d02pq4c0](https://uptobox.com/0gt4d02pq4c0)  
- Episode 9  
[https://uptobox.com/u9qhmpm3mjob](https://uptobox.com/u9qhmpm3mjob)  
- Episode 10  
[https://uptobox.com/fpf8fjhigfxr](https://uptobox.com/fpf8fjhigfxr)  
- Episode 11  
[https://uptobox.com/bbh4qzxi4m4r](https://uptobox.com/bbh4qzxi4m4r)  
- Episode 12  
[https://uptobox.com/btq7mfkeb2kt](https://uptobox.com/btq7mfkeb2kt)  
- Episode 13  
[https://uptobox.com/7uf76kwjbx6t](https://uptobox.com/7uf76kwjbx6t)  
- Episode 14  
[https://uptobox.com/lcl7exqyi8cx](https://uptobox.com/lcl7exqyi8cx)  
- Episode 15  
[https://uptobox.com/w1cx8keavlzj](https://uptobox.com/w1cx8keavlzj)  
- Episode 16  
[https://uptobox.com/xe1ygn862o6l](https://uptobox.com/xe1ygn862o6l)  
- Episode 17  
[https://uptobox.com/kp621q1zewoo](https://uptobox.com/kp621q1zewoo)  
- Episode 18  
[https://uptobox.com/9ksj48v8jqx0](https://uptobox.com/9ksj48v8jqx0)  
- Episode 19  
[https://uptobox.com/10zpklvbu80x](https://uptobox.com/10zpklvbu80x)  
- Episode 20  
[https://uptobox.com/bex05pwz6zo7](https://uptobox.com/bex05pwz6zo7)  
- Episode 21  
[https://uptobox.com/2ko8hqiaa7jw](https://uptobox.com/2ko8hqiaa7jw)  
- Episode 22  
[https://uptobox.com/40ehlc8qcg7z](https://uptobox.com/40ehlc8qcg7z)  

## Saison 4

1080p FRENCH
- Episode 1  
[https://uptobox.com/1fihbwucfqmc](https://uptobox.com/1fihbwucfqmc)  
- Episode 2  
[https://uptobox.com/1jkwiu1yadee](https://uptobox.com/1jkwiu1yadee)  
- Episode 3  
[https://uptobox.com/nfv2cp30qot3](https://uptobox.com/nfv2cp30qot3)  
- Episode 4  
[https://uptobox.com/0gppopxwsdxm](https://uptobox.com/0gppopxwsdxm)  
- Episode 5  
[https://uptobox.com/564107fm7oiu](https://uptobox.com/564107fm7oiu)  
- Episode 6  
[https://uptobox.com/8apxspmmouaw](https://uptobox.com/8apxspmmouaw)  
- Episode 7  
[https://uptobox.com/90rt4hjuz2bh](https://uptobox.com/90rt4hjuz2bh)  
- Episode 8  
[https://uptobox.com/3abrq23i2kos](https://uptobox.com/3abrq23i2kos)  
- Episode 9  
[https://uptobox.com/ipqrxyyg2xyd](https://uptobox.com/ipqrxyyg2xyd)  
- Episode 10  
[https://uptobox.com/5pf09bscxvif](https://uptobox.com/5pf09bscxvif)  
- Episode 11  
[https://uptobox.com/8qrzpdwec9fr](https://uptobox.com/8qrzpdwec9fr)  
- Episode 12  
[https://uptobox.com/qa0st9zvnr5l](https://uptobox.com/qa0st9zvnr5l)  
- Episode 13  
[https://uptobox.com/d04lua1ejdeu](https://uptobox.com/d04lua1ejdeu)  
- Episode 14  
[https://uptobox.com/s2m8jvqu4gej](https://uptobox.com/s2m8jvqu4gej)  
- Episode 15  
[https://uptobox.com/d6pilxevgk2f](https://uptobox.com/d6pilxevgk2f)  
- Episode 16  
[https://uptobox.com/bbicrjhdohaj](https://uptobox.com/bbicrjhdohaj)  
- Episode 17  
[https://uptobox.com/fbth1amlw0b7](https://uptobox.com/fbth1amlw0b7)  
- Episode 18  
[https://uptobox.com/0bmd1r5zf2s5](https://uptobox.com/0bmd1r5zf2s5)  
- Episode 19  
[https://uptobox.com/99hoz9p4xswt](https://uptobox.com/99hoz9p4xswt)  
- Episode 20  
[https://uptobox.com/mp6bwauofw12](https://uptobox.com/mp6bwauofw12)  
- Episode 21  
[https://uptobox.com/24tpymcumvvi](https://uptobox.com/24tpymcumvvi)  
- Episode 22  
[https://uptobox.com/ws30crpo7a4d](https://uptobox.com/ws30crpo7a4d)  

## Saison 5

1080p MULTi
- Episode 1  
[https://uptobox.com/e6ccdrq2vnct](https://uptobox.com/e6ccdrq2vnct)  
- Episode 2  
[https://uptobox.com/hy9h7a7db7fz](https://uptobox.com/hy9h7a7db7fz)  
- Episode 3  
[https://uptobox.com/n442ous0ymbo](https://uptobox.com/n442ous0ymbo)  
- Episode 4  
[https://uptobox.com/ezu0ct7olfvz](https://uptobox.com/ezu0ct7olfvz)  
- Episode 5  
[https://uptobox.com/sbjgyqy5dciy](https://uptobox.com/sbjgyqy5dciy)  
- Episode 6  
[https://uptobox.com/k3httzzq241v](https://uptobox.com/k3httzzq241v)  
- Episode 7  
[https://uptobox.com/w06ddbwe1kh2](https://uptobox.com/w06ddbwe1kh2)  
- Episode 8  
[https://uptobox.com/wgkhb9b4uy8h](https://uptobox.com/wgkhb9b4uy8h)  
- Episode 9  
[https://uptobox.com/gvgyh3m8uai8](https://uptobox.com/gvgyh3m8uai8)  
- Episode 10  
[https://uptobox.com/8tq3yklszblw](https://uptobox.com/8tq3yklszblw)  
- Episode 11  
[https://uptobox.com/j7ac3frbnwcg](https://uptobox.com/j7ac3frbnwcg)  
- Episode 13  
[https://uptobox.com/2lflvk9jqicv](https://uptobox.com/2lflvk9jqicv)  
- Episode 14  
[https://uptobox.com/qorunads7xcj](https://uptobox.com/qorunads7xcj)  
- Episode 15  
[https://uptobox.com/ed58vjaeti2d](https://uptobox.com/ed58vjaeti2d)  
- Episode 16  
[https://uptobox.com/0jarsjxfj68u](https://uptobox.com/0jarsjxfj68u)  
- Episode 18  
[https://uptobox.com/b3tm6e65cuue](https://uptobox.com/b3tm6e65cuue)  
- Episode 19  
[https://uptobox.com/n9db8998ygvn](https://uptobox.com/n9db8998ygvn)  
- Episode 20  
[https://uptobox.com/0320ebqentn4](https://uptobox.com/0320ebqentn4)  
- Episode 22  
[https://uptobox.com/wqmgav2z7voj](https://uptobox.com/wqmgav2z7voj)  
- Episode 23  
[https://uptobox.com/80qm9mkfg8jn](https://uptobox.com/80qm9mkfg8jn)  

1080p FRENCH
- Episode 12  
[https://uptobox.com/g6scz4pmpiys](https://uptobox.com/g6scz4pmpiys)  
- Episode 17  
[https://uptobox.com/5v5pqnf826tr](https://uptobox.com/5v5pqnf826tr)  
- Episode 21  
[https://uptobox.com/htyzahhse3b4](https://uptobox.com/htyzahhse3b4)  

## Saison 6

1080p MULTi
- Episode 1  
[https://uptobox.com/6ue2550ebhds](https://uptobox.com/6ue2550ebhds)  
- Episode 2  
[https://uptobox.com/6z91mis9hqgl](https://uptobox.com/6z91mis9hqgl)  
- Episode 3  
[https://uptobox.com/s5l4q2x79csr](https://uptobox.com/s5l4q2x79csr)  
- Episode 4  
[https://uptobox.com/etjrafjmr3co](https://uptobox.com/etjrafjmr3co)  
- Episode 5  
[https://uptobox.com/1nqpd6nf7oko](https://uptobox.com/1nqpd6nf7oko)  
- Episode 6  
[https://uptobox.com/in27bw91m131](https://uptobox.com/in27bw91m131)  
- Episode 7  
[https://uptobox.com/2yinvw4683l6](https://uptobox.com/2yinvw4683l6)  
- Episode 8  
[https://uptobox.com/dri9exk23xc2](https://uptobox.com/dri9exk23xc2)  
- Episode 9  
[https://uptobox.com/zf46zz4uxxqg](https://uptobox.com/zf46zz4uxxqg)  
- Episode 10  
[https://uptobox.com/ukpaakurbmew](https://uptobox.com/ukpaakurbmew)  
- Episode 11  
[https://uptobox.com/l6jekq65vpfz](https://uptobox.com/l6jekq65vpfz)  
- Episode 12  
[https://uptobox.com/acdr7z43t7os](https://uptobox.com/acdr7z43t7os)  
- Episode 13  
[https://uptobox.com/h4yay5nks3jv](https://uptobox.com/h4yay5nks3jv)  
- Episode 14  
[https://uptobox.com/s3wyyte9htck](https://uptobox.com/s3wyyte9htck)  
- Episode 15  
[https://uptobox.com/m0t8bdlbp3l7](https://uptobox.com/m0t8bdlbp3l7)  
- Episode 16  
[https://uptobox.com/tl9dfu73z3p8](https://uptobox.com/tl9dfu73z3p8)  
- Episode 17  
[https://uptobox.com/bw6uwq9pvp3z](https://uptobox.com/bw6uwq9pvp3z)  
- Episode 18  
[https://uptobox.com/e9jemz0tj4mr](https://uptobox.com/e9jemz0tj4mr)  
- Episode 19  
[https://uptobox.com/1yngg7woktgk](https://uptobox.com/1yngg7woktgk)  
- Episode 20  
[https://uptobox.com/b0q73oyexn84](https://uptobox.com/b0q73oyexn84)  
- Episode 21  
[https://uptobox.com/50ij5augbk8y](https://uptobox.com/50ij5augbk8y)  
- Episode 22  
[https://uptobox.com/cfgl8q42xq7r](https://uptobox.com/cfgl8q42xq7r)  
- Episode 23  
[https://uptobox.com/dm8xgc6xr3l9](https://uptobox.com/dm8xgc6xr3l9)  
<!-- charmed.md -->
