---
title: "Atypical"
created_at: 2021-04-12
updated_at: 2021-04-12
---

## Saison 1

720p FRENCH
- Episode 1  
[https://uptobox.com/z2tdalnkvplx](https://uptobox.com/z2tdalnkvplx)  
- Episode 2  
[https://uptobox.com/8o1whbykmah7](https://uptobox.com/8o1whbykmah7)  
- Episode 3  
[https://uptobox.com/9aomfoc7v55e](https://uptobox.com/9aomfoc7v55e)  
- Episode 4  
[https://uptobox.com/79yxnv8emlfd](https://uptobox.com/79yxnv8emlfd)  
- Episode 5  
[https://uptobox.com/r42htc0ornpq](https://uptobox.com/r42htc0ornpq)  
- Episode 6  
[https://uptobox.com/iqooimc95f4j](https://uptobox.com/iqooimc95f4j)  
- Episode 7  
[https://uptobox.com/25r4l9f2yzmh](https://uptobox.com/25r4l9f2yzmh)  
- Episode 8  
[https://uptobox.com/7xmylsobi98z](https://uptobox.com/7xmylsobi98z)  

720p VOSTFR
- Episode 1  
[https://uptobox.com/wibkwkei9c6w](https://uptobox.com/wibkwkei9c6w)  
- Episode 2  
[https://uptobox.com/glgzpgbljl1q](https://uptobox.com/glgzpgbljl1q)  
- Episode 3  
[https://uptobox.com/bx9i61s2u4zw](https://uptobox.com/bx9i61s2u4zw)  
- Episode 4  
[https://uptobox.com/amukp3mozmar](https://uptobox.com/amukp3mozmar)  
- Episode 5  
[https://uptobox.com/8m2y8eca0f5r](https://uptobox.com/8m2y8eca0f5r)  
- Episode 6  
[https://uptobox.com/n0p0m1p9ne2o](https://uptobox.com/n0p0m1p9ne2o)  
- Episode 7  
[https://uptobox.com/jl4biqc9o1vs](https://uptobox.com/jl4biqc9o1vs)  
- Episode 8  
[https://uptobox.com/or0sxl7310w6](https://uptobox.com/or0sxl7310w6)  

## Saison 2

720p FRENCH
- Episode 1  
[https://uptobox.com/4kddfbsk2xxe](https://uptobox.com/4kddfbsk2xxe)  
- Episode 2  
[https://uptobox.com/nk6zezkng03w](https://uptobox.com/nk6zezkng03w)  
- Episode 3  
[https://uptobox.com/p9dkurc2g87d](https://uptobox.com/p9dkurc2g87d)  
- Episode 4  
[https://uptobox.com/vnu7yuc80llu](https://uptobox.com/vnu7yuc80llu)  
- Episode 5  
[https://uptobox.com/97zxsuucv57w](https://uptobox.com/97zxsuucv57w)  
- Episode 6  
[https://uptobox.com/2w11fsfyti9q](https://uptobox.com/2w11fsfyti9q)  
- Episode 7  
[https://uptobox.com/gzhsntcv4azt](https://uptobox.com/gzhsntcv4azt)  
- Episode 8  
[https://uptobox.com/t8l2qaywgfae](https://uptobox.com/t8l2qaywgfae)  
- Episode 9  
[https://uptobox.com/tvlecjz606f8](https://uptobox.com/tvlecjz606f8)  
- Episode 10  
[https://uptobox.com/1pet8v5dt2fp](https://uptobox.com/1pet8v5dt2fp)  

720p VOSTFR
- Episode 1  
[https://uptobox.com/17wv7pkd8r58](https://uptobox.com/17wv7pkd8r58)  
- Episode 2  
[https://uptobox.com/23p5ahlk2ul4](https://uptobox.com/23p5ahlk2ul4)  
- Episode 3  
[https://uptobox.com/bwwsmpmpqpr4](https://uptobox.com/bwwsmpmpqpr4)  
- Episode 4  
[https://uptobox.com/a03if6qatfyu](https://uptobox.com/a03if6qatfyu)  
- Episode 5  
[https://uptobox.com/u6f48kjr8fcz](https://uptobox.com/u6f48kjr8fcz)  
- Episode 6  
[https://uptobox.com/g6wasjubt7ug](https://uptobox.com/g6wasjubt7ug)  
- Episode 7  
[https://uptobox.com/s7d1omwx26ou](https://uptobox.com/s7d1omwx26ou)  
- Episode 8  
[https://uptobox.com/45rogc8kktn4](https://uptobox.com/45rogc8kktn4)  
- Episode 9  
[https://uptobox.com/uhywxrtk7pix](https://uptobox.com/uhywxrtk7pix)  
- Episode 10  
[https://uptobox.com/0pfouz1e0my0](https://uptobox.com/0pfouz1e0my0)  

## Saison 3

1080p MULTi
- Episode 1  
[https://uptobox.com/jjw3tzn7yg6h](https://uptobox.com/jjw3tzn7yg6h)  
- Episode 2  
[https://uptobox.com/ve03jpc4u2j9](https://uptobox.com/ve03jpc4u2j9)  
- Episode 3  
[https://uptobox.com/69c6j1dvlchj](https://uptobox.com/69c6j1dvlchj)  
- Episode 4  
[https://uptobox.com/0n0ixki5cvlm](https://uptobox.com/0n0ixki5cvlm)  
- Episode 5  
[https://uptobox.com/vk6fgh3s4jtk](https://uptobox.com/vk6fgh3s4jtk)  
- Episode 6  
[https://uptobox.com/ymjpfuvrslzn](https://uptobox.com/ymjpfuvrslzn)  
- Episode 7  
[https://uptobox.com/5zq3b9g5ql9r](https://uptobox.com/5zq3b9g5ql9r)  
- Episode 8  
[https://uptobox.com/lkynup82fmvh](https://uptobox.com/lkynup82fmvh)  
- Episode 9  
[https://uptobox.com/dx5egdzmqpoi](https://uptobox.com/dx5egdzmqpoi)  
- Episode 10  
[https://uptobox.com/gmay5nu5h66e](https://uptobox.com/gmay5nu5h66e)  
<!-- atypical.md -->
